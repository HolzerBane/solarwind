#include "ShotPlasma.h"
#include <Weapon\WeaponBase.h>

#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreEntity.h>
#include <OGRE\OgreParticleSystem.h>
#include <OIS\OIS.h>

using namespace Actor;
using namespace Ogre;

ShotPlasma::ShotPlasma( Ogre::SceneManager& sceneManager, NxScene& nxScene, const ActorGroup& group  )
	: ShotBase( )
{
	static unsigned pId = 0;
	
	m_node = sceneManager.getRootSceneNode()->createChildSceneNode();
	m_pSys = sceneManager.createParticleSystem("plasma" + Ogre::StringConverter::toString(pId++), "plasma");
	m_node->attachObject( m_pSys );
	bindActor( m_pSys, nxScene, group );
	kill();
}

void ShotPlasma::activateAt( const Vector3& start,  const Vector3& velocity,  const Vector3& force  )
{
	if ( !m_alive )
	{
		setup( Vector3(0,0,0), velocity, start, Vector3(0,0,0) );
		m_node->attachObject( m_pSys );
		m_node->setPosition( m_attributes.position );
		m_alive = true;
		m_actor->wakeUp();
		m_actor->clearActorFlag( NX_AF_DISABLE_COLLISION );
	}
}

void ShotPlasma::kill()
{
	m_alive = false;
	m_pSys->detachFromParent();
	m_actor->putToSleep( );
	m_actor->raiseActorFlag( NX_AF_DISABLE_COLLISION );
	if ( m_parent )
		m_parent->shotKilled();
}

void ShotPlasma::animate( const float dt)
{
	m_attributes.position += m_attributes.velocity * dt;
	m_node->setPosition( m_attributes.position );

}

void ShotPlasma::contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel )
{
	kill();
}
