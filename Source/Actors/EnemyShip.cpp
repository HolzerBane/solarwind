#include "EnemyShip.h"
#include "ShotPlasma.h"
#include <Weapon\WeaponBase.h>
#include <Weapon\PlasmaGun.h>
#include <Engine\PhysicsEntity.h>
#include <Engine\PhysicsListener.h>
#include <SceneEntity\HUD.h>

#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreEntity.h>
#include <OGRE\OgreParticleSystem.h>
#include <OGRE\OgreParticleEmitter.h>

#include <OIS\OIS.h>

#pragma warning ( disable : 4482 ) 

using namespace Actor;
using namespace Ogre;

EnemyShip::EnemyShip(
		Ogre::SceneManager& sceneManager, 
		NxScene& nxScene, 
		Engine::PhysicsListener& phyL, 
		Ogre::SceneNode& player )
	: ActorBase( Vector3(0, 0, 0), Vector3(0, 0, 0), Vector3(-20, 0, 0), Vector3(0, 0, 0) )
	, m_throttle(5.f)
	, m_maxSpeed(30.0f)
	, m_phyListener(phyL)
	, m_hp(100)
	, m_playerNode( player )
{
	try
	{
  
	// create ship
	SceneNode* rootNode = sceneManager.getRootSceneNode();
	m_node = rootNode->createChildSceneNode();
	m_node->rotate( Ogre::Vector3(0.0f, 1.0f, 0.f), Ogre::Radian(Ogre::Math::PI / 2.f) );
	m_node->rotate( Ogre::Vector3(0.0f, 0.0f, 1.f), Ogre::Radian(0.3f) );
	m_node->scale( Ogre::Vector3(0.08f) );
	m_attributes.position = Ogre::Vector3(10.f, 0.f, 0.f);
	Entity* ss = sceneManager.getEntity("enemyShip");
	ss->setCastShadows(true);
	m_node->setPosition( m_attributes.position);
	m_node->attachObject(ss);

	bindActor( ss, nxScene, ActorGroup::Enemy );

	// create jet
	Vector3 jetPosition( m_attributes.position );
	jetPosition.y += 14;
	jetPosition.z -= 40;
	jetPosition.x -= 3;

	// create weapon placeholders

	Vector3 front = m_attributes.position;
	Ogre::SceneNode* wn =  m_node->createChildSceneNode();
	m_weapon = std::make_shared< Weapon::PlasmaGun >( sceneManager, nxScene, wn, ActorGroup::EnemyShot );
	std::for_each(m_weapon->beginShots(), m_weapon->endShots(), 
		[this]( ShotBaseRef& sb)
	{
		m_phyListener.registerUpdater( sb );
	});

	// create damage sparks
	m_damageSys = sceneManager.createParticleSystem("spark" + Ogre::StringConverter::toString(m_phyId), "spark");
	m_damageSys->getEmitter(0)->setEnabled( false );
	m_damageNode = m_node->createChildSceneNode();
	m_damageNode->setPosition( m_node->getPosition() );
	m_damageNode->attachObject(m_damageSys);

	// create on death animation
	m_explosionSys = sceneManager.createParticleSystem("EnemyExp", "explosion");
	m_explosionSys->getEmitter(0)->setTimeToLive( 0.2f );
	m_explosionSys->getEmitter(0)->setEnabled( false );

	setLifeState( true );

	}
	catch( Ogre::Exception e)
	{
		LogManager::getSingletonPtr()->logMessage( e.getFullDescription() );
	}
}

void EnemyShip::shoot ( )
{
	Ogre::Vector3 dir = ( m_playerNode.getPosition() - m_node->getPosition() );
	dir.normalise();
	m_weapon->fire( dir * 100.f );
	
}

void EnemyShip::animate( const float dt  )
{
	if ( !m_alive ) return;

	m_attributes.velocity += m_attributes.force;

	// maximize
	m_attributes.velocity.x = Ogre::Math::Clamp( m_attributes.velocity.x, -m_maxSpeed, m_maxSpeed );
	m_attributes.velocity.y = Ogre::Math::Clamp( m_attributes.velocity.y, -m_maxSpeed, m_maxSpeed );
	m_attributes.velocity.z = Ogre::Math::Clamp( m_attributes.velocity.z, -m_maxSpeed, m_maxSpeed );

	doLinearMotion( dt );
	m_node->setPosition( m_attributes.position );
	m_node->rotate( Ogre::Vector3( 0, 1, 0 ), Ogre::Radian( 0.1f ) ) ;
	//m_node->roll( - Ogre::Radian( m_attributes.velocity.y - m_prevY ) / 40.f );
	//m_prevY = m_attributes.velocity.y;

	shoot();
	m_weapon->animate(dt);
		
	// dissipation
	m_attributes.velocity *= 0.9f;
	m_attributes.force = Vector3(0.0f);
	m_attributes.position.z = 0.0f;

	if ( m_hp < 0)
	{
		setLifeState( false );
		m_node->detachAllObjects();
		m_node->removeAllChildren();
		m_node->attachObject(m_explosionSys);
		m_explosionSys->getEmitter(0)->setEnabled( true );
		m_explosionSys->getEmitter(0)->setMaxDuration( 1.f );
	}
}

void EnemyShip::contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel )
{
	if ( !m_alive ) return;

	Vector3 diff = otherPos - m_attributes.position;

	m_attributes.force -=  diff;
	m_attributes.force *= 50;
	 
	m_damageNode->setPosition( m_attributes.position + diff * 2.f  );
	m_damageSys->getEmitter(0)->setEnabled( true );
	m_damageSys->getEmitter(0)->setMaxDuration(0.2f);

	float damage = (m_attributes.velocity - otherVel).length();
	damage *= 0.5f;

	if ( other == Actor::ActorGroup::Chunk)
	{
		damage *= 0.2f;
	}

	m_hp -= (unsigned)damage;
}