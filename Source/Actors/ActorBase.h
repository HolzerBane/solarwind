#pragma once
#include <OGRE\OgreVector3.h>
#include <Engine\types.h>

namespace Ogre
{
	class SceneNode;
}

namespace Actor
{

class ActorBase 
{
public:

	struct Attributes
	{
		Attributes(
			const Ogre::Vector3& forc, 
			const Ogre::Vector3& vel, 
			const Ogre::Vector3& pos, 
			const Ogre::Vector3& rot )
			: force(forc)
			, velocity(vel)
			, position(pos)
			, rotation(rot)
		{ }

		Ogre::Vector3    force;
		Ogre::Vector3    velocity;
		Ogre::Vector3    position;
		Ogre::Vector3    rotation;
	};

	ActorBase()
		: m_attributes( Ogre::Vector3(0.0f), Ogre::Vector3(0.0f), Ogre::Vector3(0.0f), Ogre::Vector3(0.0f))
		, m_alive(false)
		, m_node(nullptr)
	{ }

	ActorBase( 
		const Ogre::Vector3& forc, 
		const Ogre::Vector3& vel, 
		const Ogre::Vector3& pos, 
		const Ogre::Vector3& rot )
		: m_attributes( forc, vel, pos, rot)
		, m_alive(false)
		, m_node(nullptr)
	{ }


	void doLinearMotion(const float dt)
	{
		m_attributes.position += m_attributes.velocity * dt; 
	}

	void setup(
		const Ogre::Vector3& forc, 
		const Ogre::Vector3& vel, 
		const Ogre::Vector3& pos, 
		const Ogre::Vector3& rot )
	{
		m_attributes.force	  = forc;
		m_attributes.velocity = vel;
		m_attributes.position = pos;
		m_attributes.rotation = rot;
	}


	bool isAlive() const { return m_alive; }
	void setLifeState( const bool val) { m_alive = val;}
	Attributes& getAttributes() { return m_attributes; }
	Ogre::SceneNode* getNode() { return m_node; }

	virtual ~ActorBase(){ };

protected:

	bool					m_alive;
	Ogre::SceneNode*		m_node;
	Attributes				m_attributes;
};


}