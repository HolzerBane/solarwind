#include "PlayerShip.h"
#include "ShotPlasma.h"
#include <Weapon\WeaponBase.h>
#include <Weapon\PlasmaGun.h>
#include <Engine\PhysicsEntity.h>
#include <Engine\PhysicsListener.h>
#include <SceneEntity\HUD.h>

#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreEntity.h>
#include <OGRE\OgreParticleSystem.h>
#include <OGRE\OgreParticleEmitter.h>

#include <OIS\OIS.h>

#pragma warning ( disable : 4482 ) 

using namespace Actor;
using namespace Ogre;

PlayerShip::PlayerShip( Ogre::SceneManager& sceneManager, NxScene& nxScene, Engine::PhysicsListener& phyL, SceneEntity::HUD& hud )
	: ActorBase( Vector3(0, 0, 0), Vector3(0, 0, 0), Vector3(-20, 0, 0), Vector3(0, 0, 0) )
	, m_throttle(5.f)
	, m_maxSpeed(30.0f)
	, m_prevY(0.f)
	, m_wasKey(false)
	, m_phyListener(phyL)
	, m_hp(100)
	, m_hud(hud)
{
	try
	{
  
	// create ship
	SceneNode* rootNode = sceneManager.getRootSceneNode();
	m_node = rootNode->createChildSceneNode();
	m_node->rotate( Ogre::Vector3(0.0f, 1.0f, 0.f), Ogre::Radian(Ogre::Math::PI / 2.f) );
	m_node->scale( Ogre::Vector3(0.08f) );
	Entity* ss = sceneManager.getEntity("playerShip");
	ss->setCastShadows(true);
	m_node->setPosition( m_attributes.position);
	m_node->attachObject(ss);

	bindActor( ss, nxScene, ActorGroup::Player );

	// create jet
	Vector3 jetPosition( m_attributes.position );
	jetPosition.y += 14;
	jetPosition.z -= 40;
	jetPosition.x -= 3;

	m_jetNode[0] = m_node->createChildSceneNode();
	m_jetPsys[0] = sceneManager.createParticleSystem("leftJet", "jet");
	m_jetNode[0]->attachObject(m_jetPsys[0]);
	m_jetNode[0]->setPosition( jetPosition );

	jetPosition.x += 20;
	m_jetNode[1] = m_node->createChildSceneNode();
	m_jetPsys[1] = sceneManager.createParticleSystem("rightJet", "jet");
	m_jetNode[1]->attachObject(m_jetPsys[1]);
	m_jetNode[1]->setPosition( jetPosition );

	// create weapon placeholders

	Vector3 front = m_attributes.position;
	//front.y += 15;
	front.x += 20;
	Ogre::SceneNode* wn =  m_node->createChildSceneNode();
	wn->setPosition( front );
	m_weapons[ WeaponPos::Front ] = std::make_shared< Weapon::PlasmaGun >( sceneManager, nxScene, wn, ActorGroup::PlayerShot );
	std::for_each( m_weapons[ WeaponPos::Front ]->beginShots(), m_weapons[ WeaponPos::Front ]->endShots(), 
		[this]( ShotBaseRef& sb)
	{
		m_phyListener.registerUpdater( sb );
	});

	// create damage sparks
	m_damageSys = sceneManager.createParticleSystem("spark" + Ogre::StringConverter::toString(m_phyId), "spark");
	m_damageSys->getEmitter(0)->setEnabled( false );
	m_damageNode = m_node->createChildSceneNode();
	m_damageNode->setPosition( m_node->getPosition() );
	m_damageNode->attachObject(m_damageSys);

	// create on death animation
	m_explosionSys = sceneManager.createParticleSystem("PlayerExp", "explosion");
	m_explosionSys->getEmitter(0)->setTimeToLive( 0.2f );
	m_explosionSys->getEmitter(0)->setEnabled( false );

	setLifeState( true );

	m_hud.reportHP( m_hp );

	}
	catch( Ogre::Exception e)
	{
		LogManager::getSingletonPtr()->logMessage( e.getFullDescription() );
	}
}

void PlayerShip::shoot ( WeaponPos weapon )
{
	m_weapons[ weapon ]->fire( Vector3( 100.f, 0.f, 0.f ) );
}

void PlayerShip::onKeys( const char* keymap )
{
	if ( !m_alive ) return;

	if( keymap[ OIS::KC_W] )
	{
		m_attributes.force.y = m_throttle;
	}

	if( keymap[ OIS::KC_S] )
	{
		m_attributes.force.y = -m_throttle;
	}

	if( keymap[ OIS::KC_D] )
	{
		m_attributes.force.x = m_throttle;
	}

	if( keymap[ OIS::KC_A] )
	{
		m_attributes.force.x = -m_throttle;
	}

	if ( keymap[ OIS::KC_RCONTROL] )
	{
		shoot( Front );
	}

	if (m_attributes.force.length() > 0)
	{
		m_wasKey = true;
	}
}

void PlayerShip::onMouse( const OIS::MouseState& ms  )
{
	if ( !m_alive ) return;

	if ( !m_wasKey)
	{
		m_attributes.force.x =  ms.X.rel * 0.05f;
		m_attributes.force.y = -ms.Y.rel * 0.05f;

		if ( ms.buttonDown( OIS::MouseButtonID::MB_Left ) )
		{
			shoot( Front );
		}
	}
}

void PlayerShip::animate( const float dt  )
{
	if ( !m_alive ) return;

	m_attributes.velocity += m_attributes.force;

	// maximize
	m_attributes.velocity.x = Ogre::Math::Clamp( m_attributes.velocity.x, -m_maxSpeed, m_maxSpeed );
	m_attributes.velocity.y = Ogre::Math::Clamp( m_attributes.velocity.y, -m_maxSpeed, m_maxSpeed );
	m_attributes.velocity.z = Ogre::Math::Clamp( m_attributes.velocity.z, -m_maxSpeed, m_maxSpeed );

	doLinearMotion( dt );
	m_node->setPosition( m_attributes.position );
	m_node->roll( - Ogre::Radian( m_attributes.velocity.y - m_prevY ) / 40.f );
	m_prevY = m_attributes.velocity.y;


	std::for_each( m_weapons.begin(), m_weapons.end(), [&dt]( const Weapon::WeaponBaseRef& weapon )
	{
		if ( weapon )
		{
			weapon->animate(dt);
		}
	}
	);
	
	// dissipation
	m_attributes.velocity *= 0.9f;
	m_attributes.force = Vector3(0.0f);
	m_coolState += dt;
	m_wasKey = false;
	m_attributes.position.z = 0.0f;

	if ( m_hp < 0)
	{
		setLifeState( false );
		m_node->detachAllObjects();
		m_node->removeAllChildren();
		m_node->attachObject(m_explosionSys);
		m_explosionSys->getEmitter(0)->setEnabled( true );
		m_explosionSys->getEmitter(0)->setMaxDuration( 1.f );
		m_hud.reportGameOver();
	}
}

void PlayerShip::contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel )
{
	if ( !m_alive ) return;

	Vector3 diff = otherPos - m_attributes.position;

	m_attributes.force -=  diff;
	m_attributes.force *= 50.f;
	 
	m_damageNode->setPosition( m_attributes.position + diff * 2.f  );
	m_damageSys->getEmitter(0)->setEnabled( true );
	m_damageSys->getEmitter(0)->setMaxDuration(0.2f);

	float damage = (m_attributes.velocity - otherVel).length();
	damage *= 0.5f;

	if ( other == Actor::ActorGroup::Chunk)
	{
		damage *= 0.2f;
	}

	m_hp -= (unsigned)damage;
	m_hud.reportHP( m_hp );
}