#pragma once

#include "ActorBase.h"

#include <Engine\types.h>
#include <Engine\IListener.h>
#include <Engine\IAnimator.h>
#include <Engine\PhysicsEntity.h>

#include <array>

#pragma warning ( disable : 4482 ) 

namespace SceneEntity
{
	class HUD;
}

namespace Ogre
{
	class SceneManager;
	class SceneNode;
	class ParticleSystem;
}

namespace Engine
{
	class PhysicsListener;
}

namespace OIS
{
	class MouseState;
}

class NxScene;

namespace Actor
{

class ShotPlasma;

class PlayerShip 
	: public ActorBase
	, public Engine::IListener
	, public Engine::IAnimator
	, public Engine::PhysicsEntity
{

public:

	enum WeaponPos
	{
		Front,
		Count
	};

	PlayerShip( Ogre::SceneManager& sceneManager, NxScene& nxScene, Engine::PhysicsListener& phyL, SceneEntity::HUD& hud );

	void onKeys( const char* keymap );
	void onMouse( const OIS::MouseState&  );
	void animate( const float );

	void shoot ( WeaponPos weapon );

	void contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel );

private:

	SceneEntity::HUD&		m_hud;

	Ogre::ParticleSystem*	m_jetPsys[2];
	Ogre::SceneNode*		m_jetNode[2];

	std::array< Weapon::WeaponBaseRef, WeaponPos::Count>	m_weapons;
	
	Engine::PhysicsListener&	m_phyListener;
	Ogre::ParticleSystem*		m_damageSys;
	Ogre::SceneNode*			m_damageNode;
	Ogre::ParticleSystem*		m_explosionSys;

	float m_coolDown;
	float m_coolState;

	bool m_wasKey;

	float m_prevMouseX;
	float m_prevMouseY;

	float m_prevY;
	float m_throttle;
	float m_maxSpeed;

	int m_hp;
};


}