#pragma once
#include "ShotBase.h"
#include <Engine\PhysicsEntity.h>

namespace Ogre
{
	class SceneManager;
	class ParticleSystem;
	class Entity;
}

namespace LevelSection
{
	class MeteorSwarm;
}

namespace Actor
{

class Debris 
	: public ShotBase
{
public:
	Debris( Ogre::SceneManager& sceneManager, NxScene& nxScene, LevelSection::MeteorSwarm&  ms);

	~Debris()
	{ }

	void activateAt( const Ogre::Vector3& start, const Ogre::Vector3& velocity, const Ogre::Vector3& force = Ogre::Vector3(0.f)  );
	void kill();
	void animate( const float dt);

	void update();
	void contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel );

private:
	LevelSection::MeteorSwarm&	m_parent;
	Ogre::Entity*				m_entity;
	Ogre::ParticleSystem*		m_pSys;
};

}