#pragma once
#include "ShotBase.h"

namespace Ogre
{
	class SceneManager;
	class ParticleSystem;
}

namespace Actor
{

class ShotPlasma : 
	  public ShotBase

{
public:
	ShotPlasma( Ogre::SceneManager& sceneManager, NxScene& nxScene, const ActorGroup& );
	void activateAt( const Ogre::Vector3& start, const Ogre::Vector3& velocity, const Ogre::Vector3& force );
	void kill();
	void animate( const float dt);

	void contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel ) override;


private:

	Ogre::ParticleSystem* m_pSys;
};

}