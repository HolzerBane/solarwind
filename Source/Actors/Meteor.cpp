#include "Meteor.h"

#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreEntity.h>
#include <Engine\PhysicsEntity.h>
#include <LevelSection\MeteorSwarm.h>

#include <random>

using namespace Ogre;
using namespace Actor;

#pragma warning ( disable : 4482 ) 

Meteor::Meteor( Ogre::SceneManager& sceneManager, NxScene& nxScene, LevelSection::MeteorSwarm&  ms  )
	: m_parent( ms )
{
	// create
	static unsigned mId = 0;
	SceneNode* rootNode = sceneManager.getRootSceneNode();
	m_node = rootNode->createChildSceneNode();
	m_node->rotate( Ogre::Vector3(0.0f, 1.0f, 0.f), Ogre::Radian(Ogre::Math::PI / 2.f) );
	m_node->scale( Ogre::Vector3(0.08f) );
	m_entity = sceneManager.getEntity("meteor")->clone( "meteor" + Ogre::StringConverter::toString(mId++) );
	m_entity->setCastShadows(true);
	m_node->attachObject( m_entity );
	bindActor( m_entity, nxScene, ActorGroup::Enemy );
	m_node->detachAllObjects( );
}

void Meteor::activateAt( const Ogre::Vector3& start, const Ogre::Vector3& velocity, const Ogre::Vector3& force )
{
	if ( !m_alive )
	{
		setup( Vector3(0, 0, 0), velocity, start, Vector3( rand() % 3 / 20.f, rand() % 3  / 10.f, rand() % 3  / 15.f) );
		m_alive = true;
		m_node->attachObject( m_entity );
	}
}

void Meteor::kill()
{
	m_alive = false;
	m_node->detachAllObjects();
}

void Meteor::animate( const float dt)
{
	doLinearMotion(dt);
	m_node->setPosition( m_attributes.position );
	m_node->rotate( m_attributes.rotation, Ogre::Radian(m_attributes.rotation.x) );
}


void Meteor::update()
{
	if ( isAlive())
	{
		PhysicsEntity::update();
	}	
}


void Meteor::contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel )
{
	Vector3 diff = otherPos - m_attributes.position;
	if ( diff.length() > 10 ) return;
	if ( other == ActorGroup::Enemy )
	{
		diff *= 5;
	}

	m_attributes.velocity -=  diff ;

	if ( other == Actor::ActorGroup::PlayerShot )
	{
		m_parent.meteorGotDestroyed( *this );
	}

}
