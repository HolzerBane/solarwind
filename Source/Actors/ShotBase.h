#pragma once
#include "ActorBase.h"
#include <Engine\IAnimator.h>
#include <Engine\PhysicsEntity.h>

namespace Ogre
{
	class Vector3;
}

namespace Weapon
{
	class WeaponBase;
}

namespace Actor
{

class ShotBase : public ActorBase, public Engine::IAnimator,  public Engine::PhysicsEntity

{
public:

	 ShotBase() : m_parent(nullptr) { };

	virtual ~ShotBase() { };
	virtual void activateAt( const Ogre::Vector3& start, const Ogre::Vector3& velocity,  const Ogre::Vector3& force ) = 0;
	virtual void kill() = 0;
	virtual void contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel ){};
	
	void setParent( Weapon::WeaponBase* wb) { m_parent = wb; }
protected:
	Weapon::WeaponBase*	m_parent;

};

}