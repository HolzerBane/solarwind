#include "Debris.h"

#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreEntity.h>
#include <Engine\PhysicsEntity.h>
#include <LevelSection\MeteorSwarm.h>

#include <random>

using namespace Ogre;
using namespace Actor;

#pragma warning ( disable : 4482 ) 

Debris::Debris( Ogre::SceneManager& sceneManager, NxScene& nxScene, LevelSection::MeteorSwarm&  ms  )
	: m_parent( ms )
{
	static unsigned debrisId = 0;

	m_node = sceneManager.getRootSceneNode()->createChildSceneNode( "debris" + Ogre::StringConverter::toString(debrisId++) );
	m_node->scale( Ogre::Vector3(0.03f) );
	m_entity = sceneManager.getEntity("meteor")->clone( "meteord" + Ogre::StringConverter::toString(debrisId++) );
	m_entity->setCastShadows(true);
	m_node->attachObject( m_entity );
	// create
	bindActor( m_entity, nxScene, ActorGroup::Chunk, true );
	m_alive = true;
}

void Debris::activateAt( const Ogre::Vector3& start, const Ogre::Vector3& velocity, const Ogre::Vector3& force )
{
	if ( !m_alive )
	{
		setup( Vector3(0, 0, 0), velocity, start, Vector3( rand() % 3 / 20.f, rand() % 3  / 10.f, rand() % 3  / 15.f) );
		m_alive = true;
		m_node->attachObject( m_entity );
	}
}

void Debris::kill()
{
	m_alive = false;
	m_node->detachAllObjects();
}

void Debris::animate( const float dt)
{
	// handled by physx
}


void Debris::update()
{
	if ( isAlive() )
	{
		PhysicsEntity::update();

		if ( m_node->getPosition().y < -90 )
		{
			m_parent.debrisGotDestroyed( *this );	
		}
	}	
}


void Debris::contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel )
{
	Vector3 diff = otherPos - m_attributes.position;
	if ( diff.length() > 10 ) return;

	m_parent.debrisGotDestroyed( *this );
}
