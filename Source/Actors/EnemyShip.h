#pragma once

#include "ActorBase.h"

#include <Engine\types.h>
#include <Engine\IListener.h>
#include <Engine\IAnimator.h>
#include <Engine\PhysicsEntity.h>

#include <array>
namespace SceneEntity
{
	class HUD;
}

namespace Ogre
{
	class SceneManager;
	class SceneNode;
	class ParticleSystem;
}

namespace Engine
{
	class PhysicsListener;
}


class NxScene;

namespace Actor
{

class ShotPlasma;

class EnemyShip 
	: public ActorBase
	, public Engine::IAnimator
	, public Engine::PhysicsEntity
{

public:

	EnemyShip( 
		Ogre::SceneManager& sceneManager, 
		NxScene& nxScene, 
		Engine::PhysicsListener& phyL, 
		Ogre::SceneNode& player );

	void animate( const float );

	void shoot ( );

	void contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel );

private:

	Ogre::ParticleSystem*	m_jetPsys[2];
	Ogre::SceneNode*		m_jetNode[2];

	Engine::PhysicsListener&	m_phyListener;
	Ogre::ParticleSystem*		m_damageSys;
	Ogre::SceneNode*			m_damageNode;
	Ogre::SceneNode&			m_playerNode;
	Ogre::ParticleSystem*		m_explosionSys;
	
	Weapon::WeaponBaseRef		m_weapon;

	float m_throttle;
	float m_maxSpeed;

	int m_hp;
};


}