#include "MeteorSwarm.h"

#include <Actors\Meteor.h>
#include <Actors\Debris.h>
#include <Engine\PhysicsListener.h>
#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreParticleSystem.h>
#include <OGRE\OgreParticleEmitter.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreEntity.h>
#include <SceneEntity\HUD.h>

#include <NxVec3.h>

using namespace Ogre;
using namespace LevelSection; 

MeteorSwarm::MeteorSwarm( Ogre::SceneManager& sm, NxScene& physXScene, Engine::PhysicsListener& phyL, const unsigned num, const Ogre::Vector3& avgVel, SceneEntity::HUD& hud  )
	: m_avgVel( avgVel )
	, m_phyListener( phyL )
	, m_scene( sm )
	, m_physXScene( physXScene )
	, m_hud(hud)
	, m_levelTime(0)
	, m_level(1)
{
	m_meteors.resize(num);

	m_explosionSys = sm.createParticleSystem("meteorExp", "explosion");
	m_explosionSys->getEmitter(0)->setEnabled( false );

	m_explosionNode = sm.getRootSceneNode()->createChildSceneNode();
	m_explosionNode->attachObject(m_explosionSys);

	for ( unsigned i = 0; i < num; ++i )
	{
		m_meteors[i] = std::make_shared< Actor::Meteor >( sm, physXScene, *this );
		initMeteor(i);
	}
}

void MeteorSwarm::meteorGotDestroyed( Actor::Meteor& m )
{
	// report it
	m_hud.reportScore( (int)m.getAttributes().velocity.length() );

	// blast force
	const unsigned bf = 40;
	const float bfHalf = bf / 2.f;
	const unsigned maxDebris = 8;

	static unsigned debrisId = 0;
	m.kill();

	m_explosionNode->setPosition( m.getNode()->getPosition() );
	m_explosionSys->getEmitter(0)->setEnabled( true );
	m_explosionSys->getEmitter(0)->setMaxDuration(0.4f);

	for ( int num = 0; num < maxDebris; ++num )
	{
		m_debrisTest = std::make_shared< Actor::Debris >( m_scene, m_physXScene, *this ); 
		NxVec3 pos( m.getAttributes().position.ptr() );
		NxVec3 origPos( pos );

		pos.x += rand() % 8 - 4;
		pos.y += rand() % 8 - 4;
		m_debrisTest->getActor().setGlobalPosition( pos  );
		m_debrisTest->getActor().setAngularVelocity( NxVec3( (float)(rand() % 10) )   );

		NxVec3 vel = (pos - origPos) * 10 ;

		m_debrisTest->getActor().setLinearVelocity( vel );
		m_phyListener.registerUpdater( m_debrisTest );
	}
}

void MeteorSwarm::debrisGotDestroyed( Actor::Debris& m )
{
	Vector3 pos = m.getNode()->getPosition();
	m.getActor().userData = (void*)DEAD_ID;
	m.kill();
	// destroys the Debris object
	m_phyListener.unregisterUpdater( m.getId() );


}

void MeteorSwarm::initMeteor( const unsigned i )
{
	float randomVelX = - (rand() % (int)m_avgVel.x + m_avgVel.x / 2.f );
	float randomVelY =   m_avgVel.y / 2.f - rand() % (int)m_avgVel.y;
	float randomPos = 60.f - rand() % 120 ;
	m_meteors[i]->activateAt( Vector3( 60, randomPos, 0.f ), Vector3( randomVelX, randomVelY, 0.f ) ); 
}

void MeteorSwarm::increaseDifficulity()
{
	m_meteors.push_back( std::make_shared< Actor::Meteor >( m_scene, m_physXScene, *this ) );
	initMeteor( m_meteors.size() - 1 );

	m_avgVel += 1;

	m_hud.reportLevel( ++m_level );
}

void MeteorSwarm::animate( const float dt)
{
	m_levelTime += dt;

	if ( m_levelTime > 10 )
	{
		m_levelTime = 0.f;
		increaseDifficulity();
	}

	for( unsigned i = 0; i < m_meteors.size(); ++i )
	{
		if ( m_meteors[i]->isAlive())
		{
			m_meteors[i]->animate(dt);

			if ( m_meteors[i]->getAttributes().position.x < -60 || 
				 m_meteors[i]->getAttributes().position.y < -60 || 
				 m_meteors[i]->getAttributes().position.y >  60)
			{
				m_meteors[i]->kill();
			}
		}
		else
		{
				initMeteor(i);		
		}
	}
}
