#pragma once

#include <Engine\types.h>
#include <Engine\IAnimator.h>
#include <Engine\IPhyUpdater.h>
#include <OGRE\OgreVector3.h>

#include <vector>

namespace Engine
{
	class PhysicsListener;	
}

namespace SceneEntity
{
	class HUD;
}

namespace Ogre
{
	class SceneManager;
	class Vector3;
	class ParticleSystem;
}

namespace Actor
{
	class Debris;
	class Meteor;
}

class NxScene;

namespace LevelSection
{

class MeteorSwarm : public Engine::IAnimator
{
public:

	typedef std::vector< Actor::MeteorRef > MeteorVector;
	typedef std::vector< Engine::PhysicsEntityRef > MeteorPhyVector;

	MeteorSwarm( Ogre::SceneManager& sm,  NxScene& physXScene,  Engine::PhysicsListener& phyL , const unsigned num, const Ogre::Vector3& avgVel, SceneEntity::HUD& hud  );
	void initMeteor( const unsigned i);
	void animate( const float dt);

	MeteorVector::iterator beginMeteors() { return m_meteors.begin();	}
	MeteorVector::iterator endMeteors() {	return m_meteors.end();		}

	void meteorGotDestroyed( Actor::Meteor& m );
	void debrisGotDestroyed( Actor::Debris& m );
	void increaseDifficulity();

private:

	float				m_levelTime;
	int					m_level;
	SceneEntity::HUD&	m_hud;

	Ogre::SceneManager&	m_scene;
	NxScene&			m_physXScene;

	Ogre::ParticleSystem*		m_explosionSys;
	Ogre::SceneNode*			m_explosionNode;

	Ogre::Vector3	m_avgVel;
	MeteorVector	m_meteors;
	Engine::PhysicsListener& m_phyListener;

	Actor::DebrisRef m_debrisTest;
};

}