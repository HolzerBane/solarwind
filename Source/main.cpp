#include <SDKDDKVer.h>
#include <stdio.h>
#include <tchar.h>

#include "Engine\Application.h"

// disable unstandard enum usage warning
#pragma warning ( disable : 4482 ) 

int _tmain(int argc, _TCHAR* argv[])
{
	Engine::Application app;
	bool ok = app.init();
	app.start();

	return 0;
}