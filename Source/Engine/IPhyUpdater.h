#pragma once
#include "types.h"

namespace Ogre
{
	class Vector3;
}

namespace Engine
{

class IPhyUpdater
{
public:
	IPhyUpdater()
	{
		static unsigned phId = 0;
		m_phyId = phId++;
	}

	const unsigned getId() const { return m_phyId; }
	
	virtual ~IPhyUpdater( ){};
	virtual void update( ) = 0;

	virtual void contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel ) = 0;

protected:
	unsigned m_phyId;
};

}