#pragma once

namespace Engine
{

class IService 
{
public:
	virtual ~IService() { } 

	virtual bool init() = 0;
	virtual bool shutdown() = 0;
};

}