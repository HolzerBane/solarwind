#pragma once
#include <Ogre\OgreFrameListener.h>
#define OIS_DYNAMIC_LIB
#include <OIS\OIS.h>
#include "AppState.h"

#include "types.h"

namespace Ogre
{
	class RenderWindow;
	class SceneManager;
}

namespace Engine
{

class InputListener : public Ogre::FrameListener
{
public:

	InputListener( Ogre::RenderWindow* renderWindow, Ogre::SceneManager* sceneManager, AppState& appState );
	/* Ogre::FrameListener override */
	bool frameStarted( const Ogre::FrameEvent& evt ) override;

	void registerListener( const IListenerRef& lp );
	void registerAnimator( const IAnimatorRef& ap );

	void reset();

private:

	AppState& m_appState;

	float m_timeSinceStart;
	float m_timeSinceFrame;
	float m_frameRate;

	OIS::InputManager*	m_OISInputManager;
	OIS::Keyboard*		m_keyboard;
	OIS::Mouse*			m_mouse;

	std::vector< IListenerRef >		m_listeners;
	std::vector< IAnimatorRef >		m_animators;
};

}
