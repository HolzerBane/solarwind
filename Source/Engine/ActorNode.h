#pragma once

#include "ActorEvent.h"
#include "types.h"

#include <vector>

namespace Engine
{

class ActorNode
{
public:

	ActorNode( ActorNode& parent );

	typedef std::vector< ActorNode >	ChildVector;
	typedef unsigned					ChildIndex;

	ChildIndex addChild( const ActorNode& child );
	bool removeChild( const ChildIndex index );

	bool report( const ActorNode& sender, const ActorEvent event );

private:

	Actor::ActorRef	m_actor;
	ActorNode&		m_parent;
	ChildVector		m_children;
};

}