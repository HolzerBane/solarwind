#pragma once

namespace Engine
{

enum ActorEvent
{
	Destroyed = 0,
	GotHit
};

}

