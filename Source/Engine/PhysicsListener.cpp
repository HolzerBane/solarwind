#include "PhysicsListener.h"
#include "IPhyUpdater.h"

#include <Ogre\OgreLogManager.h>
#include <Ogre\OgreVector3.h>
#include <NxPhysics.h>
#include <NxPhysicsSDK.h>
#include <NxScene.h>

using namespace Ogre;
using namespace Engine;

#pragma warning ( disable : 4482 ) 

PhysicsListener::PhysicsListener(void)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing Physx ***");
	m_PhysicsSDK = NxCreatePhysicsSDK(NX_PHYSICS_SDK_VERSION);
	m_PhysicsSDK->getFoundationSDK().setErrorStream( &m_errorStr );
	if (!m_PhysicsSDK)
		return;

	m_PhysicsSDK->getFoundationSDK().getRemoteDebugger()->connect("localhost", 5425);

	// nagyon debug
	m_PhysicsSDK->setParameter(NX_VISUALIZE_COLLISION_SHAPES, 1);
	m_PhysicsSDK->setParameter(NX_VISUALIZE_ACTOR_AXES, 1);
	m_PhysicsSDK->setParameter(NX_VISUALIZE_CONTACT_POINT, 1);
	m_PhysicsSDK->setParameter(NX_VISUALIZE_CONTACT_NORMAL, 1);

	m_PhysicsSDK->setParameter(NX_SKIN_WIDTH, 0.01f);

	initScene();

	Ogre::LogManager::getSingletonPtr()->logMessage("*** Initialized Physx ***");
}

void PhysicsListener::initScene()
{
	NxSceneDesc sceneDesc;
	sceneDesc.simType = NX_SIMULATION_HW;
	sceneDesc.gravity = NxVec3(0,-90.8f,0);
	sceneDesc.groundPlane = false;

	m_PhysicsScene = m_PhysicsSDK->createScene(sceneDesc);	
	if(!m_PhysicsScene)
	{ 
		sceneDesc.simType = NX_SIMULATION_SW; 
		m_PhysicsScene = m_PhysicsSDK->createScene(sceneDesc);  
		if(!m_PhysicsScene) 
			return;
	}

	// Create the default material
	NxMaterial* defaultMaterial = m_PhysicsScene->getMaterialFromIndex(0); 
	defaultMaterial->setRestitution(0.5f);
	defaultMaterial->setStaticFriction(0.5f);
	defaultMaterial->setDynamicFriction(0.5f);

	// TEMP
	using namespace Actor;
	m_PhysicsScene->setActorGroupPairFlags( ActorGroup::Player, ActorGroup::Enemy, NX_NOTIFY_ON_START_TOUCH );
	m_PhysicsScene->setActorGroupPairFlags( ActorGroup::Player, ActorGroup::EnemyShot, NX_NOTIFY_ON_START_TOUCH );
	m_PhysicsScene->setActorGroupPairFlags( ActorGroup::Player, ActorGroup::Chunk, NX_NOTIFY_ON_START_TOUCH );
	m_PhysicsScene->setActorGroupPairFlags( ActorGroup::Player, ActorGroup::PowerUp, NX_NOTIFY_ON_START_TOUCH );
	m_PhysicsScene->setActorGroupPairFlags( ActorGroup::Enemy, ActorGroup::Enemy, NX_NOTIFY_ON_START_TOUCH );
	m_PhysicsScene->setActorGroupPairFlags( ActorGroup::Enemy, ActorGroup::Chunk, NX_NOTIFY_ON_START_TOUCH );
	m_PhysicsScene->setActorGroupPairFlags( ActorGroup::Enemy, ActorGroup::PlayerShot, NX_NOTIFY_ON_START_TOUCH );

	m_PhysicsScene->setUserContactReport( this );
}

bool PhysicsListener::frameStarted(const Ogre::FrameEvent& evt)
{
	m_PhysicsScene->simulate(evt.timeSinceLastFrame);
	m_PhysicsScene->flushStream();
	while (!m_PhysicsScene->fetchResults(NX_RIGID_BODY_FINISHED, false));
	
	std::for_each( m_updaters.begin(), m_updaters.end(), [this]( const UpdaterMap::value_type& updater )
	{
		if (updater.second)
			updater.second->update();
	}
	);

	std::for_each( m_markedForDelete.begin(), m_markedForDelete.end(), [this]( const unsigned id )
	{
		m_updaters.erase( id );
	}
	);

	m_markedForDelete.clear();
	
	return true;

}

void PhysicsListener::registerUpdater( const IPhyUpdaterRef& lp )
{
	m_updaters.insert( std::make_pair( lp->getId(), lp ));
}

void PhysicsListener::unregisterUpdater( const unsigned id )
{
	m_markedForDelete.push_back( id );
}

void PhysicsListener::onContactNotify(NxContactPair& pair, NxU32 events)
{
	bool valid = true;

	NxActor& a1 = *pair.actors[0];
	NxActor& a2 = *pair.actors[1];

	unsigned a1Id = (unsigned)a1.userData;
	unsigned a2Id = (unsigned)a2.userData;

	if ( a1Id == DEAD_ID)
	{
		m_PhysicsScene->releaseActor( a1 );
		valid = false;
	}

	if ( a2Id == DEAD_ID)
	{
		m_PhysicsScene->releaseActor( a2 );
		valid = false;
	}

	if ( valid )
	{
		auto ag1 = (Actor::ActorGroup)a1.getGroup();
		auto ag2 = (Actor::ActorGroup)a2.getGroup();

		if ( m_updaters[a1Id] )
		m_updaters[a1Id]->contact( 
			(Actor::ActorGroup)a2.getGroup(), 
			Ogre::Vector3(a2.getGlobalPosition().get()), 
			Ogre::Vector3(a2.getLinearVelocity().get())
			);
		if ( m_updaters[a2Id] )
		m_updaters[a2Id]->contact( 
			(Actor::ActorGroup)a1.getGroup(), 
			Ogre::Vector3(a1.getGlobalPosition().get()), 
			Ogre::Vector3(a1.getLinearVelocity().get())
			);
	}
}

void PhysicsListener::reset()
{
	m_updaters.clear();
	m_markedForDelete.clear();

	m_PhysicsSDK->releaseScene( *m_PhysicsScene );
	initScene();
}
