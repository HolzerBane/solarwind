#include "Application.h"
#include "Media.h"
#include "Scene.h"
#include "View.h"
#include "InputListener.h"
#include "PhysicsListener.h"
#include "IAnimator.h"

#include <SceneEntity\Backdrop.h>
#include <SceneEntity\HUD.h>
#include <Actors\PlayerShip.h>
#include <Actors\EnemyShip.h>
#include <LevelSection\MeteorSwarm.h>
#include <Ogre\OgreRoot.h>
#include <OGRE\OgreWindowEventUtilities.h>
#include <OGRE\OgreRenderWindow.h>

#include <Actors\Meteor.h>

using namespace Engine;

#define METEORS 1

#pragma warning ( disable : 4482 ) 

Application::Application()
{

}

bool Application::init()
{

	bool ok = true;
	m_appRoot.reset( new AppRootRef::element_type("","") ); 

#ifdef _DEBUG
	m_appRoot->loadPlugin("RenderSystem_Direct3D9_d");
	m_appRoot->loadPlugin("Plugin_ParticleFX_d");
#else
	m_appRoot->loadPlugin("RenderSystem_Direct3D9");
	m_appRoot->loadPlugin("Plugin_ParticleFX");
#endif

	Ogre::RenderSystemList list = m_appRoot->getAvailableRenderers(); 
	m_appRoot->setRenderSystem( list.at(0) );
	m_appRoot->initialise( false );

	m_scene.reset( new ScenePtr::element_type(*m_appRoot) );
	m_view.reset(  new ViewPtr::element_type(*m_appRoot) );
	m_media.reset( new MediaPtr::element_type(*m_appRoot) );
	
	ok = ok && m_view->init();
	ok = ok && m_media->init();
	ok = ok && m_scene->init();

	m_input.reset( new InputPtr::element_type( m_view->getRenderWindow(), &m_scene->getSceneManager(), m_appState ) );
	m_physics.reset( new PhysicsPtr::element_type(  ) );
    Ogre::LogManager::getSingletonPtr()->logMessage( "Creating HUD" );
	m_hud.reset( new SceneEntity::HUDPtr::element_type(  ) );
    Ogre::LogManager::getSingletonPtr()->logMessage( "Creating camera" );

	m_view->createCamera( m_scene->getSceneManager(), Ogre::Vector3(0, 0, 100), Ogre::Vector3(0, 0, 0) ); 

	m_appRoot->addFrameListener( m_input.get() );
	m_appRoot->addFrameListener( m_physics.get() );
	m_appRoot->addFrameListener( m_hud.get() );

	return true;
}

bool Application::shutdown()
{
	return true;
}

void Application::setupTest()
{
	try
	{
	// TEMP
	// testing

	// reseting initial state
    Ogre::LogManager::getSingletonPtr()->logMessage( "Resetting state" );
	m_input->reset();
	m_physics->reset();
	m_scene->reset();
	m_media->reset();

    Ogre::LogManager::getSingletonPtr()->logMessage( "Initing actors" );

	SceneEntity::Backdrop bd(m_scene->getSceneManager(), Ogre::Vector3(0, 20, 0) );

	m_player = std::make_shared< Actor::PlayerShip >(  m_scene->getSceneManager(), m_physics->getNxScene(), *m_physics, *m_hud );
	//auto enemy = std::make_shared< Actor::EnemyShip >( m_scene->getSceneManager(), m_physics->getNxScene(), *m_physics, *m_player->getNode() );
	//enemy->setLifeState( true );
#if METEORS
	std::shared_ptr< LevelSection::MeteorSwarm > meteors =
		std::make_shared< LevelSection::MeteorSwarm >( 
		m_scene->getSceneManager(),
		m_physics->getNxScene(), 
		*m_physics, 
		10, 
		Ogre::Vector3( 70, 10, 0), 
		*m_hud );

	m_input->registerAnimator( meteors );

	std::for_each( meteors->beginMeteors(), meteors->endMeteors(), [this] ( Actor::MeteorRef& meteor)
	{
		m_physics->registerUpdater( meteor );
	}
	);
#endif

    Ogre::LogManager::getSingletonPtr()->logMessage( "Registering listeners" );

	m_input->registerListener( m_player );
	m_input->registerAnimator( m_player );
	//m_input->registerAnimator( enemy );

	// set physics

	m_physics->registerUpdater( m_player );



	}
	catch( Ogre::Exception e)
	{
		Ogre::LogManager::getSingletonPtr()->logMessage( e.getFullDescription() );
	}

	m_appState = AppState::GameOn;
	m_hud->reportNewGame();
}


void Application::start()
{
    Ogre::LogManager::getSingletonPtr()->logMessage( "App start" );

	setupTest();
	// start rendering!
	while(true)
	{
		if( m_view->getRenderWindow()->isClosed())
		{
			m_appRoot->shutdown();
			break;
		}

		Ogre::WindowEventUtilities::messagePump();
		
		if ( !m_player->isAlive() && m_appState == AppState::GameOn )
		{
			m_appState = AppState::GameOver;
		}

		if ( m_appState == AppState::Retry )
		{
			setupTest();
		}

		if( !m_appRoot->renderOneFrame())
		{
			m_appRoot->shutdown();
			break;
		}
	}

	//m_appRoot->startRendering();

}
