#pragma once

namespace Engine
{

class IAnimator
{
public:
	virtual ~IAnimator(){};
	virtual void animate( const float dt ) = 0;
};


}