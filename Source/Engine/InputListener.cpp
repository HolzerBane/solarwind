#include "InputListener.h"
#include "IListener.h"
#include "IAnimator.h"

#include <OGRE\OgreRenderWindow.h>
#include <OIS\OISInputManager.h>
#include <OIS\OISKeyboard.h>
#include <OIS\OISMouse.h>
#include <Ogre\OgreLogManager.h>
#include <assert.h>

#pragma warning ( disable : 4482 ) 

using namespace Engine;
using namespace Ogre;

InputListener::InputListener( Ogre::RenderWindow* renderWindow, Ogre::SceneManager* sceneManager, AppState& appState )
	: m_timeSinceStart(0.f)
	, m_frameRate( 1.f / 60.f ) 
	, m_timeSinceFrame( 0.f )
	, m_appState( appState )
{
	assert( renderWindow );
	assert( sceneManager );
	LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
		
	OIS::ParamList pl;

	size_t windowHnd = 0;
	renderWindow->getCustomAttribute("WINDOW", &windowHnd);
	std::ostringstream windowHndStr;
	windowHndStr << windowHnd;
	pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

	m_OISInputManager = OIS::InputManager::createInputSystem( pl );

	m_keyboard = static_cast<OIS::Keyboard*>(m_OISInputManager->createInputObject( OIS::OISKeyboard, false ));
	m_mouse = static_cast<OIS::Mouse*>(m_OISInputManager->createInputObject( OIS::OISMouse, false ));
	
	LogManager::getSingletonPtr()->logMessage("*** Initialized OIS ***");
}

void InputListener::registerListener( const IListenerRef& lp )
{
	m_listeners.push_back( lp );
}

void InputListener::registerAnimator( const IAnimatorRef& ap )
{
	m_animators.push_back( ap );
}

bool InputListener::frameStarted( const Ogre::FrameEvent& evt )
{
	m_timeSinceStart += evt.timeSinceLastFrame;

	if ( m_timeSinceFrame < m_frameRate)
	{
		m_timeSinceFrame += evt.timeSinceLastFrame;
		return true;
	}

	m_keyboard->capture();
	m_mouse->capture();

	char keys[256];
	m_keyboard->copyKeyStates( keys );
	const OIS::MouseState& mouse = m_mouse->getMouseState();

	std::for_each( m_listeners.begin(), m_listeners.end(), [this, &keys, &mouse, &evt]( const IListenerRef& listener )
	{
		listener->onKeys( keys );
		listener->onMouse( mouse );
	}
	);

	std::for_each( m_animators.begin(), m_animators.end(), [this, &keys, &mouse, &evt]( const IAnimatorRef& animator )
	{
		animator->animate( m_timeSinceFrame );
	}
	);

	m_timeSinceFrame = 0.f;

	// handle user keys

	if ( keys[OIS::KC_ESCAPE] )
	{
		return false;
	}

	if ( keys[ OIS::KC_F2] && m_appState == AppState::GameOver )
	{
		m_appState = AppState::Retry;
	}

	return true;
}

void InputListener::reset()
{
	m_listeners.clear();
	m_animators.clear();
}

