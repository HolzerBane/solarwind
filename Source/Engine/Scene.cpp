#include "Scene.h"

#include <OGRE\OgreRoot.h>
#include <OGRE\OgreCamera.h>
#include <OGRE\OgreMovablePlane.h>
#include <OGRE\OgreMeshManager.h>
#include <OGRE\OgreEntity.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreSceneManager.h>

using namespace Engine;
using namespace Ogre;

Scene::Scene( Ogre::Root& or )
	: m_appRoot(or)
	, m_scene( *m_appRoot.createSceneManager(0, "Default") )
	, m_sceneRoot( *m_scene.getRootSceneNode())
{
}

Scene::~Scene()
{

}

bool Scene::init()
{

	// init lights
	Light* light1 = m_scene.createLight("pointlight1");
	light1->setType(Light::LT_POINT);
	light1->setAttenuation(3250.f, 1.0f, 0.0014f, 0.000007f);
	light1->setDebugDisplayEnabled(true);
	light1->setDiffuseColour( 1.f, 1.f, 1.f);
	light1->setCastShadows(true);
	light1->setPosition(1.f, 1.f, 0.f);
	light1->setDirection(1.f, 1.f, 0.f);

	m_scene.setAmbientLight(ColourValue(0.6f,0.6f,0.6f,1.0f));

	return true;
}

void Scene::reset()
{
	m_scene.clearScene();
	m_sceneRoot = *m_scene.getRootSceneNode();
	init();
}

bool Scene::shutdown()
{
	return true;
}

void Scene::createGround()
{
	MovablePlane* plane = new MovablePlane("GroundPlane");
	plane->d = 0;
	plane->normal = Vector3::UNIT_Y;
	MeshManager::getSingleton().createPlane(
		"GroundPlane", 
		ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
		*plane,
		500,// x-size
		500,// y-size
		100,	// x-segments
		100,  // y-segments
		true,
		1,
		10, // u-tile
		10, // v-tile
		Vector3::UNIT_Z);
	Entity* planeEnt = m_scene.createEntity( "GroundPlane", "GroundPlane" );
	planeEnt->setMaterialName("Ground");
	planeEnt->setCastShadows(false);
	m_scene.getRootSceneNode()->createChildSceneNode()->attachObject(planeEnt);
}