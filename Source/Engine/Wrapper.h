#pragma once

namespace Engine
{

template < class TWrapped, class TWrapper>
class Wrapper : public TWrapper
{
public:

	typedef TWrapped WrappedType;
	typedef TWrapper WrapperType;

	Wrapper( const TWrapped& wrapped )
		: m_wrapped( wrapped )
	{
	
	}

	TWrapped& accessWrapped() { return m_wrapped; }

private:
	TWrapped& m_wrapped;

};

}