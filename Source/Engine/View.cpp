#include "View.h"
#include <Ogre\OgreRoot.h>
#include <OGRE\OgreRenderWindow.h>

using namespace Engine;
using namespace Ogre;

View::View( Ogre::Root& or )
	: m_appRoot(or)
{
	m_window = nullptr;
}

bool View::init()
{
	m_window = m_appRoot.createRenderWindow("Lots of Meteors are Coming from the Right Side of the Screen", 1000, 600, false);

	return m_window != nullptr;
}

void View::createCamera(  Ogre::SceneManager& scene,  const Vector3& position, const Vector3& lookAt )
{
	m_camera = scene.createCamera("Main Camera");       
	m_camera->setPosition( position ); 
	m_camera->lookAt( lookAt );
	m_camera->setFarClipDistance(1000.f);
	m_camera->setNearClipDistance(0.1f);

	m_viewport = m_window->addViewport(m_camera);
	m_window->setActive(true); 
}

bool View::shutdown()
{
	return true;
}