#include "Media.h"
#include <Ogre\OgreRoot.h>
#include <OGRE\OgreResourceGroupManager.h>
#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreEntity.h>

using namespace Engine;
using namespace Ogre;

Media::Media( Ogre::Root& or )
	: m_appRoot(or)
{

}

Media::~Media()
{

}


bool Media::init()
{
	try
	{
	ResourceGroupManager::getSingleton().addResourceLocation("media",
							"FileSystem",
							"General", 
							false);

	ResourceGroupManager::getSingleton().initialiseAllResourceGroups(); 

	// init objects
	Ogre::SceneManager* scene = m_appRoot.getSceneManager( "Default" );
	scene->createEntity("playerShip", "ssOgreuw.mesh");
	scene->createEntity("meteor", "meteorOgre.mesh");
	scene->createEntity("enemyShip", "enemyOgre.mesh");

	}
	catch( Ogre::Exception e)
	{
		LogManager::getSingletonPtr()->logMessage( e.getFullDescription() );
	}

	return true;
}

void Media::reset()
{
	init();
}

bool Media::shutdown()
{
	return true;
}