#pragma once
#include <memory>
#include <array>

#include "IService.h"
#include "types.h"
#include <OGRE\OgreFrameListener.h>

namespace Ogre
{
	class Root;
	class Light;
	class SceneNode;
	class SceneManager;
}

namespace Engine
{

class Scene : public IService, public Ogre::FrameListener
{
public:

	Scene( Ogre::Root& or );
	~Scene( );

	bool init() override;
	bool shutdown() override;

	void createGround();
	void reset();

	Ogre::SceneManager& getSceneManager() { return m_scene; }

private:

	Ogre::Root&			m_appRoot;
	Ogre::SceneManager&	m_scene;

	Ogre::SceneNode&	m_sceneRoot;

	std::array< Ogre::Light*,  1 >	m_lights;
};

}


