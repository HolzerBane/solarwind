#pragma once
#include <memory>

// for dead actors

#pragma warning( disable : 4146)
static const unsigned DEAD_ID = -1u;

namespace Engine
{
	class Media;
	class Scene;
	class View;
	class InputListener;
	class PhysicsListener;

	class PhysicsEntity;

	class IListener;
	class IAnimator;
	class IPhyUpdater;
}

namespace Ogre
{
	class Root;
	class SceneManager;
	class RenderWindow;
	class IListener;
}

namespace Actor
{
	class ActorBase;
	class Meteor;
	class ShotBase;
	class Debris;
	class PlayerShip;

	enum ActorGroup
	{
		Player = 1,
		Enemy,
		Chunk,
		PowerUp,
		PlayerShot,
		EnemyShot
	};
}

namespace Weapon
{
	class WeaponBase;
}
namespace SceneEntity
{
	class HUD;
}

namespace Engine
{

// unique ptr would need full type ...
typedef std::shared_ptr< Scene >				ScenePtr;
typedef std::shared_ptr< View >					ViewPtr;
typedef std::shared_ptr< Media >				MediaPtr;
typedef std::shared_ptr< InputListener >		InputPtr;
typedef std::shared_ptr< PhysicsListener >		PhysicsPtr;

typedef std::shared_ptr< PhysicsEntity >		PhysicsEntityRef;

typedef std::shared_ptr< IListener >			IListenerRef;
typedef std::shared_ptr< IAnimator >			IAnimatorRef;
typedef std::shared_ptr< IPhyUpdater >			IPhyUpdaterRef;

typedef std::shared_ptr< Ogre::Root	>			AppRootRef;
typedef std::shared_ptr< Ogre::SceneManager >	SceneManagerRef;
typedef std::shared_ptr< Ogre::RenderWindow >	RenderWindowRef;

}

namespace Actor
{
	typedef std::shared_ptr< Actor::ActorBase >		ActorRef;
	typedef std::shared_ptr< Actor::Meteor >		MeteorRef;
	typedef std::shared_ptr< Actor::Debris >		DebrisRef;
	typedef std::shared_ptr< Actor::ShotBase >		ShotBaseRef;
	typedef std::shared_ptr< Actor::PlayerShip >	PlayerRef;
}

namespace Weapon
{
	typedef std::shared_ptr< Weapon::WeaponBase >	WeaponBaseRef;
}

namespace SceneEntity
{
	typedef std::shared_ptr< SceneEntity::HUD >		HUDPtr;
}