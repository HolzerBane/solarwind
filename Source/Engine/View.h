#pragma once
#include <memory>

#include "IService.h"
#include "types.h"

namespace Ogre
{
	class Root;
	class Vector3;
	class Camera;
	class Viewport;
}

namespace Engine
{

class View : public IService
{
public:
	View( Ogre::Root& or );

	View( );

	bool init() override;
	bool shutdown() override;

	void createCamera( Ogre::SceneManager& scene, const Ogre::Vector3& position, const Ogre::Vector3& lookAt );

	Ogre::RenderWindow* getRenderWindow( ) { return m_window; }

private:

	Ogre::Root&			m_appRoot;

	Ogre::RenderWindow* m_window;
	Ogre::Camera*		m_camera;
	Ogre::Viewport*		m_viewport;

};

}


