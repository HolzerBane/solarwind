#pragma once
#include <memory>

#include "IService.h"
#include "types.h"
#include "AppState.h"

namespace Engine
{

class Application : public IService
{
public:
	Application();

	bool init()		override;
	bool shutdown() override;

	void start();

	void setupTest();

private:
	AppState	m_appState;

	AppRootRef	m_appRoot;
	ViewPtr		m_view;
	MediaPtr	m_media;
	ScenePtr	m_scene;
	InputPtr	m_input;
	PhysicsPtr	m_physics;

	Actor::PlayerRef	m_player;

	SceneEntity::HUDPtr m_hud;

};

}


