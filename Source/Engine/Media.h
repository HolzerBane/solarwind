#pragma once
#include <memory>

#include "IService.h"
#include "types.h"

namespace Ogre
{
	class Root;
}

namespace Engine
{

class Media : public IService
{
public:

	Media( Ogre::Root& or );
	~Media();

	Media( );

	bool init() override;
	bool shutdown() override;
	void reset();

private:

	Ogre::Root&			m_appRoot;
};

}


