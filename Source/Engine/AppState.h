#pragma once

namespace Engine
{
	enum AppState
	{
		NotStarted,
		Started,
		GameOn,
		GameOver,
		Retry
	};
}


