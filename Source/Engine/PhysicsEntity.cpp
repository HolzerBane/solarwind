#include "PhysicsEntity.h"
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreMovableObject.h>
#include <NxScene.h>
#include <NxVec3.h>
#include <assert.h>

using namespace Ogre;
using namespace Engine;

PhysicsEntity::PhysicsEntity()
{
	m_sceneNode = nullptr;
}

void PhysicsEntity::bindActor( 
	Ogre::MovableObject* ogreEntity, NxScene& physXScene, const Actor::ActorGroup actorGroup, bool trueSimulation )
{
	m_trueSimulation = trueSimulation;

	m_sceneNode = ogreEntity->getParentSceneNode();
	m_boxDesc.dimensions.set(NxVec3(	ogreEntity->getBoundingBox().getSize().x * m_sceneNode->getScale().x, 
										ogreEntity->getBoundingBox().getSize().y * m_sceneNode->getScale().y, 
										ogreEntity->getBoundingBox().getSize().z * m_sceneNode->getScale().z 
								  ));	
	m_boxDesc.dimensions *= 0.3f;

	if ( m_boxDesc.dimensions.magnitude() == 0 )
		m_boxDesc.dimensions = NxVec3( 1 );

	m_boxDesc.localPose.t = NxVec3(0.f, 0.f, 0.f);
	m_boxDesc.userData = (void*)m_phyId;
	m_boxDesc.group = actorGroup;

	m_actorDesc.shapes.pushBack( &m_boxDesc );
	// setting dynamic body
	m_actorDesc.body = &m_bodyDesc;
	m_actorDesc.density = 10;
	m_actorDesc.globalPose.t 	= NxVec3(	m_sceneNode->getPosition().x,
											m_sceneNode->getPosition().y,
											m_sceneNode->getPosition().z
									);

	m_actor = physXScene.createActor(m_actorDesc);
	m_actor->userData = (void*)m_phyId;
	
	if ( !m_trueSimulation )
	{
		m_actor->raiseBodyFlag( NX_BF_DISABLE_GRAVITY );
	}
	
	m_actor->setGroup( actorGroup ); 
}

void PhysicsEntity::addContact(  NxScene* physXScene, NxActor& other)
{
	physXScene->setActorPairFlags( *m_actor, other, NX_NOTIFY_ON_START_TOUCH );
}

PhysicsEntity::~PhysicsEntity(void)
{
}

NxActor& PhysicsEntity::getActor()
{ 
	assert( m_actor );
	return *m_actor;
}

void PhysicsEntity::update(void)
{
	if ( !m_trueSimulation )
	{
		Vector3 pos = m_sceneNode->_getDerivedPosition();
		m_actor->setGlobalPosition(NxVec3(pos.x,pos.y,pos.z));
	}
	else
	{
		NxVec3 gpos = (m_actor->getGlobalPosition() );
		m_sceneNode->setPosition( Vector3(gpos.get()) );
	}
}
