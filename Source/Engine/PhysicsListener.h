#pragma once
#include <Ogre\OgreFrameListener.h>
#include <NxUserOutputStream.h>
#include "types.h"
#include <NxUserContactReport.h>
#include <map>

namespace Ogre
{
	class RenderWindow;
	class SceneManager;
}

class NxPhysicsSDK;
class NxScene;

namespace Engine
{

class PhysicsListener : public Ogre::FrameListener,  public NxUserContactReport
{
public:

	PhysicsListener( );
	/* Ogre::FrameListener override */
	bool frameStarted( const Ogre::FrameEvent& evt ) override;

	void registerUpdater( const IPhyUpdaterRef& lp );
	void unregisterUpdater( const unsigned id );
	NxScene& getNxScene() { return *m_PhysicsScene; }

	void reset();

private:

	void initScene();
	void onContactNotify(NxContactPair& pair, NxU32 events);

	struct ErrorOut : public NxUserOutputStream
	{
		void reportError(NxErrorCode code, const char * message, const char *file, int line)
		{
		
		}

		NxAssertResponse reportAssertViolation(const char * message, const char *file, int line)
		{
			return NxAssertResponse();
		}
		void print(const char * message)
		{
		
		}

	};
	ErrorOut		m_errorStr;
	NxPhysicsSDK*	m_PhysicsSDK;
	NxScene*		m_PhysicsScene;

	float m_timeSinceStart;
	float m_timeSinceFrame;
	float m_frameRate;

	typedef std::map< unsigned, IPhyUpdaterRef > UpdaterMap;
	typedef std::vector< unsigned > IdVector;

	IdVector		m_markedForDelete;
	UpdaterMap		m_updaters;
};

}
