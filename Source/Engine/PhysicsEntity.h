#pragma once

#include "IPhyUpdater.h"

#include <NxPhysics.h>
#include <NxActorDesc.h>
#include <NxBoxShapeDesc.h>
#include <NxBodyDesc.h>

#include "types.h"

class NxActor;
class NxScene;

namespace Ogre
{
	class SceneNode;
	class MovableObject;
}

class NxContactPair;

namespace Engine
{

class PhysicsEntity : public IPhyUpdater
{
public:

	PhysicsEntity( );
	void bindActor( Ogre::MovableObject* ogreEntity, NxScene& physXScene, const Actor::ActorGroup actorGroup, bool trueSimulation = false);
	virtual ~PhysicsEntity();
	virtual void update();
	virtual void addContact( NxScene* physXScene, NxActor& other);
	NxActor& getActor(); 

	virtual void contact( Actor::ActorGroup other, Ogre::Vector3& otherPos, Ogre::Vector3& otherVel ) = 0;


protected:
	bool				m_trueSimulation;
	Ogre::SceneNode*    m_sceneNode;
	NxActorDesc			m_actorDesc;				
	NxBoxShapeDesc		m_boxDesc;
	NxActor*			m_actor;
	NxBodyDesc			m_bodyDesc;
};

}
