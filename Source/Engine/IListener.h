#pragma once

namespace OIS
{
	class MouseState;
}

namespace Engine
{

class IListener
{
public:
	virtual ~IListener(){}

	virtual void onKeys(  const char* keymap ) = 0;
	virtual void onMouse( const OIS::MouseState&  ) = 0;
};

}