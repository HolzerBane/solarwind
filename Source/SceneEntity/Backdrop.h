#pragma once


namespace Ogre
{
	class SceneManager;
	class BillboardSet;
	class Vector3;
}

namespace SceneEntity
{

class Backdrop
{
public:
	Backdrop( Ogre::SceneManager& sm, const Ogre::Vector3& position );

private:
	void setBackground( Ogre::SceneManager& );
	Ogre::BillboardSet* m_billBoard;
};

}