#include "Backdrop.h"
#include <OGRE\OgreBillboardSet.h>
#include <OGRE\OgreBillboard.h>
#include <OGRE\OgreSceneManager.h>
#include <OGRE\OgreMeshManager.h>
#include <OGRE\OgreSceneNode.h>
#include <OGRE\OgreEntity.h>
#include <OGRE\OgreSceneNode.h>

using namespace SceneEntity;
using namespace Ogre;

Backdrop::Backdrop( Ogre::SceneManager& sm, const Vector3& position )
{
	std::string error("");

	try
	{
		setBackground(sm);
#if 0

		m_billBoard = sm.createBillboardSet("backdrop_set", 1);
		m_billBoard->setMaterialName("Backdrop");	
		SceneNode* billboardSetNode = sm.getRootSceneNode()->createChildSceneNode();
		billboardSetNode->attachObject(m_billBoard);
		m_billBoard->setDefaultDimensions(800,600);
		m_billBoard->setBillboardType(BBT_ORIENTED_COMMON);
		m_billBoard->setBillboardOrigin(BBO_CENTER);
		m_billBoard->setCommonDirection(Vector3::UNIT_Y);
		m_billBoard->setCullIndividually(true);
		m_billBoard->setSortingEnabled(true);
		m_billBoard->setAutoextend(true);		
	

		m_billBoard->createBillboard(position);	
#endif
	}
	catch( Ogre::Exception e)
	{
		error = e.getFullDescription();
	}

}

void Backdrop::setBackground( Ogre::SceneManager& sm )
{
	MovablePlane* plane = new MovablePlane("BackPlane");
	plane->d = 0;
	plane->normal = Vector3::UNIT_Y;
	MeshManager::getSingleton().createPlane(
		"BackPlane", 
		ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		*plane,
		1000,// x-size
		600,// y-size
		1,	// x-segments
		1,  // y-segments
		false,
		1,
		1, // u-tile
		1, // v-tile
		Vector3::UNIT_Z);
	Entity* planeEnt = sm.createEntity( "BackPlane", "BackPlane" );
	
	planeEnt->setMaterialName("Backdrop");
	planeEnt->setCastShadows(false);
	SceneNode* sn = sm.getRootSceneNode()->createChildSceneNode();
	sn->rotate( Vector3(1.f, 0.f, 0.f), Ogre::Radian( Ogre::Degree( 90.f )) );
	sn->translate( Vector3( 0.f, 0.f, -500.f ) );
	sn->attachObject(planeEnt);

}

