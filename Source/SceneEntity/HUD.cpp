#include "HUD.h"
#include <OGRE\OgreOverlayManager.h>
#include <OGRE\OgreOverlayElement.h>
#include <OGRE\OgreStringConverter.h>

using namespace SceneEntity;

HUD::HUD()
	: m_playerHP(0)
	, m_timePlayed(0)
	, m_fps(0)
	, m_score(0)
	, m_timeFromSecond(0)
	, m_framesInSecond(0)
	, m_level(1)
	, m_gameover(false)
{
	Ogre::Overlay* mainOverlay = Ogre::OverlayManager::getSingleton().getByName("MainOverlay");

	mainOverlay->show();

	m_fpsText	= Ogre::OverlayManager::getSingleton().getOverlayElement("FPS");
	m_scoreText = Ogre::OverlayManager::getSingleton().getOverlayElement("SCORE");
	m_timeText	= Ogre::OverlayManager::getSingleton().getOverlayElement("TIME");
	m_hpText	= Ogre::OverlayManager::getSingleton().getOverlayElement("HP");
	m_levelText	= Ogre::OverlayManager::getSingleton().getOverlayElement("LEVEL");
	m_gameoverText = Ogre::OverlayManager::getSingleton().getOverlayElement("GAMEOVER");
	m_gameoverHelpText = Ogre::OverlayManager::getSingleton().getOverlayElement("GAMEOVERHELP");

	m_fpsText->show();	
	m_scoreText->show();
	m_timeText->show();
	m_hpText->show();	
	m_levelText->show();	
}

bool HUD::frameStarted(const Ogre::FrameEvent& evt)
{
	float dt = evt.timeSinceLastFrame; 
	m_timePlayed += dt;

	m_timeFromSecond += dt;
	m_framesInSecond++;
	if( m_timeFromSecond >= 1)
	{
		m_fps = (int)(m_framesInSecond / m_timeFromSecond);
		m_timeFromSecond = 0;
		m_framesInSecond = 0;
	}

	Ogre::OverlayManager::getSingleton().getOverlayElement("HPCAPTION")->setCaption("HULL:");
	Ogre::OverlayManager::getSingleton().getOverlayElement("FPSCAPTION")->setCaption("FPS:");
	Ogre::OverlayManager::getSingleton().getOverlayElement("TIMECAPTION")->setCaption("TIME:");
	Ogre::OverlayManager::getSingleton().getOverlayElement("SCORECAPTION")->setCaption("SCORE:");
	Ogre::OverlayManager::getSingleton().getOverlayElement("LEVELCAPTION")->setCaption("LEVEL:");
	Ogre::OverlayManager::getSingleton().getOverlayElement("INFO")->setCaption(
		"Use mouse to move and left click to shoot"
		);

	m_gameoverText->setCaption( "GAME OVER" );
	m_gameoverHelpText->setCaption( "Press F2 to retry or ESC to carry on living" );

	// color hp text
	float green = ( m_playerHP > 40 ) ? ( 1.f ) : (std::max(0.0f, m_playerHP - 30.f ) / 10.f); //std::max( 0.f, m_playerHP - 33.33f ) / 66.66f;
	float red   = ( m_playerHP < 60 ) ? ( 1.f ) : (std::max(0.0f, 60.f - m_playerHP) / 10.f); //std::max( 0.f, 66.66f - m_playerHP ) / 66.66f;
	m_hpText->setColour( Ogre::ColourValue( red, green, 0.f) );

	Ogre::ColourValue oc = m_hpText->getColour();

	m_fpsText->setCaption( Ogre::StringConverter::toString(m_fps, 4 ));
	m_scoreText->setCaption( Ogre::StringConverter::toString( m_score ) );
	m_hpText->setCaption( Ogre::StringConverter::toString( m_playerHP ) );
	m_timeText->setCaption( Ogre::StringConverter::toString( (int)m_timePlayed ) );
	m_levelText->setCaption( Ogre::StringConverter::toString( m_level ) );

	( m_gameover ) ? ( m_gameoverText->show() ) : ( m_gameoverText->hide() );
	( m_gameover ) ? ( m_gameoverHelpText->show() ) : ( m_gameoverHelpText->hide() );

	return true;

}

void HUD::reportScore( const int score )
{
	m_score += score;
}

void HUD::reportHP( const int hp )
{
	m_playerHP = hp;
}

void HUD::reportLevel( const int lvl )
{
	m_level = lvl;
}

void HUD::reportGameOver( )
{
	m_gameover = true;
}

void HUD::reportNewGame( )
{
	m_gameover = false;
	m_score = 0;
}
