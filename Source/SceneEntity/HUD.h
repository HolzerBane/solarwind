#pragma once
#include <OGRE\OgreFrameListener.h>

namespace SceneEntity
{

class HUD : public Ogre::FrameListener
{
protected:

	int		m_playerHP;
	float	m_timePlayed;
	int		m_fps;
	int		m_score;
	int		m_level;

	float m_timeFromSecond;
	float m_framesInSecond;

	bool m_gameover;

	Ogre::OverlayElement* m_fpsText;
	Ogre::OverlayElement* m_scoreText;
	Ogre::OverlayElement* m_timeText;
	Ogre::OverlayElement* m_hpText;
	Ogre::OverlayElement* m_levelText;
	Ogre::OverlayElement* m_gameoverText;
	Ogre::OverlayElement* m_gameoverHelpText;

public:
	HUD();
	bool frameStarted(const Ogre::FrameEvent& evt);

	void reportScore( const int score );
	void reportHP( const int hp );
	void reportLevel( const int lvl );
	void reportGameOver( );
	void reportNewGame( );

};

}

