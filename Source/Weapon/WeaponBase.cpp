#include "WeaponBase.h"

#include <Actors\ShotBase.h>

#include <OGRE\OgreSceneNode.h>

using namespace Weapon;
using namespace Ogre;

WeaponBase::WeaponBase()
	: m_attributes()
{

}

void WeaponBase::fire( const Ogre::Vector3& vel)
{
	if ( m_attributes.shotsAlive < m_attributes.maxShots )
	{
		for ( unsigned i = 0; i < m_attributes.maxShots; ++i )
		{
			if ( !m_shots[i]->isAlive())
			{
				m_shots[i]->activateAt( m_node->getParent()->getPosition() + m_node->getPosition(), vel, Vector3(0,0,0) );
				m_shots[i]->setParent( this );
				m_attributes.shotsAlive++;
			}
		}
	}
}

void WeaponBase::animate( const float dt )
{
	for ( unsigned i = 0; i < m_attributes.maxShots; ++i )
	{
		Actor::ShotBase& shot = *m_shots[i];

		if ( shot.isAlive() )
		{
			shot.animate(dt);
		}

		if ( shot.isAlive() && ( 
			m_shots[i]->getAttributes().position.x > 100  || 
			m_shots[i]->getAttributes().position.x < -100 || 
			m_shots[i]->getAttributes().position.y > 60   || 
			m_shots[i]->getAttributes().position.y < -60  
			 )
		   )
		{
			shot.kill();
		}
	}
}