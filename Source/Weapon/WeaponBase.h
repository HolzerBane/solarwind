#pragma once
#include <Actors\ShotBase.h>
#include <Engine\types.h>
#include <Engine\IAnimator.h>

#include <vector>


namespace Ogre
{
	class SceneNode;
	class Vector3;
}

namespace Weapon
{

class WeaponBase : public Engine::IAnimator
{
public:

	typedef std::vector< Actor::ShotBaseRef	> ShotVector;

	struct Attributes
	{
		Attributes()
			: power(1)
			, rounds(1)
			, maxShots(1)
			, shotsAlive(0)
			, coolDown(1)
			, coolState(0)
		{ }

		float		power;
		unsigned	rounds;
	
		unsigned	maxShots;
		unsigned	shotsAlive;

		float		coolDown;
		float		coolState;
	};

	WeaponBase();
	virtual ~WeaponBase(){};

	Attributes& getAttributes() { return m_attributes; }

	virtual void fire( const Ogre::Vector3& );
	virtual void animate( const float );

	ShotVector::iterator beginShots() { return m_shots.begin(); }
	ShotVector::iterator endShots() {   return m_shots.end(); }

	void shotKilled() { m_attributes.shotsAlive--; }

protected:

	Attributes			m_attributes;

	Ogre::SceneNode*	m_node;
	ShotVector			m_shots;
};


}