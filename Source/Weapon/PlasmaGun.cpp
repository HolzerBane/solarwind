#include "PlasmaGun.h"

#include <Actors\ShotPlasma.h>

using namespace Weapon;
using namespace Ogre;

PlasmaGun::PlasmaGun(){}

PlasmaGun::PlasmaGun( Ogre::SceneManager& sceneManager, NxScene& nxScene, Ogre::SceneNode* node, const Actor::ActorGroup& group )
{
	m_node = node;
	m_shots.resize( m_attributes.maxShots );

	std::for_each( m_shots.begin(), m_shots.end(), [&sceneManager, &nxScene, &group]( Actor::ShotBaseRef& shot )
	{
		shot = std::make_shared< Actor::ShotPlasma >( sceneManager, nxScene, group );
		shot->setLifeState( false );
	}
	);
}
