#pragma once
#include "WeaponBase.h"
#include <Engine\types.h>

namespace Weapon
{

class PlasmaGun : public WeaponBase
{
public:
	PlasmaGun();
	PlasmaGun( Ogre::SceneManager& sceneManager, NxScene& nxScene, Ogre::SceneNode* node, const Actor::ActorGroup& group );

};

}